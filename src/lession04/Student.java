package lession04;

import lession05.DemoStatic;

/**
 * Description
 *
 * @author Vin
 */
public class Student {
    String name;
    String code;
    String dob;
    String className;

    public Student() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public Student(String name) {
        this.name = name;
    }

    public void learn(String major) {
        System.out.println(this.name + " learn " + major);
    }

    public void watchTV() {
    }

    public void accessModifier(){
        DemoStatic demo = new DemoStatic();
        //demo.proVariable ="Protected";
        demo.pubVariable ="Public";
        //demo.defVariable ="Default";
    }

    public void play() {
    }

    // Phuong thuc danh gia xep loai hoc sinh
    public String grade(int score) {
        if (score < 5) {
            return "Trung Binh";
        } else if (score < 8) {
            return "Kha";
        } else if (score < 10) {
            return "Gioi";
        } else if (score == 10) {
            return "Xuat sac";
        }
        return "None";
    }
}

