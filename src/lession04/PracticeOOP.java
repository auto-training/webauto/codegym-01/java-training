package lession04;

public class PracticeOOP {

    public static void main(String[] args) {
        Student studentA = new Student("Tran Van A");
        studentA.play();
        studentA.watchTV();
        studentA.getName(); // String

        studentA.setName("AAA");

        studentA.learn("Toan");

        Student studentB = new Student("Nguyen Van B");

        String gradeA = studentA.grade(6);
        System.out.println("Grade A: " + gradeA);

        String gradeB = studentB.grade(10);
        System.out.println("Grade B: " + gradeB);

        Student C = new Student("Nguyen Van C");
        System.out.println("Grade C: " +C.grade(4));
    }
}
