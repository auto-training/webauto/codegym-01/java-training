package lession02;

import java.util.Arrays;

public class Loops {


    public static void main(String[] args) {
        // Vòng lặp For

            /*      for (giá tri bắt đầu, giá trị kết thúc, bước nhảy){
                        Xu ly logic
                    }
            */

        // In ra thu tu cac so tu 1 toi 10
        //        for (int i = 1; i <= 10; i++) {
        //            System.out.println("index : " + i);
        //        }


        //
        String[] names = {"Ha Anh", "Nhat Quang", "Hung", "Ha An"};
        for (int i = 0; i < 4; i++) {
            String tmpName = names[i];
            System.out.println("Your name - index: " + (i + 1) + "  - name: " + tmpName);
        }
        // For each
        System.out.println(" For each");
        for (String tmpName : names) {
            System.out.println("Your name - For each: " + tmpName);
        }

        // Lambda
        System.out.println("Lambda Java 8");
        Arrays.stream(names).forEach(tmpName -> System.out.println("Lambda -> " + tmpName));

        // While
        System.out.println("\nWhile loop:");
        int age = 10;
        int count = 0;
        while (age <= 18) { //14
            age++;
            if (age == 14) {
                count++;
                System.out.println("Count " + count);
                continue;
            }
            System.out.println("Hoc sinh: " + age);

        }
        System.out.println("\nYour age: " + age);

        // do-while
        System.out.println();
        age = 19;
        do {
            System.out.println("Hoc sinh - " + age);
        } while (age <= 18);

        // BT In ra số nguyên tố
        System.out.println("Day so nguyen to");
        int countSoLe= 0;

        for (int i = 1; i <= 1000; i++) {
            // region So Nguyen To
            //            int countNT = 0;
            //            // Duyet vong lap de tim ra so luong so chia het
            //            for (int k = 2; k <= Math.sqrt(i); k++) {
            //                if (i % k == 0) countNT++;
            //            }
            //            // Neu tong so luong chia het > 0 thi do khong phai la so nguyen to
            //            if (countNT == 0) System.out.print(i + " ");
            // endregion
            if (i % 2 != 0) {
                countSoLe ++;
                System.out.print(i + " ");
            }

            // In ra 100 so le dau tien
            if (countSoLe == 100) break;
        }


        System.out.println();

        int c = 0;
        countSoLe = 1;
        while (c <= 1000 && countSoLe <= 100){
            if (c % 2 != 0) {
                countSoLe ++;
                System.out.print(c + " ");
            }
            c++;
            //if (countSoLe == 100) break;
        }
    }

}
