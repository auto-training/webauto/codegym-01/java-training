package lession02;

public class Conditions {

    public static void main(String[] args) {
        // Cấu trúc điều kiện
        /**
         *
         *                 if(Điều kiện 01) {
         *                     Thực thi logic 01
         *                 }
         *
         *                 if(Điều kiện 01){
         *                     Thực thi logic 01
         *                 }
         *                 else  {
         *                     Thực thi logic 02
         *                 }
         *
         *                 if(Điều kiện 01){
         *                     Thực thi logic 01
         *                 }
         *                 else if( Điều kiện 02){
         *                     Thực thi logic 02
         *                 }else {
         *                     Thực thi logic 03
         *                 }
         */

        /* Xay dung chuong trinh chuyen doi, danh gia xep loai HS
                        < 5 -> TB
                        <8 TB Kha
                        < 9 Kha
                        < 10 Gioi
                        = 10 XS
        */

        float diemSo = 6;
        if (diemSo < 0 || diemSo > 10) {
            System.out.println("Diem so khong hop le");
        } else if (diemSo < 5) {
            System.out.println("Hoc luc: Trung Binh");
        } else if (diemSo < 8) {  //  = 5 ?
            System.out.println("Hoc luc : TB Kha");
        } else if (diemSo < 9) {
            System.out.println("Hoc luc: Kha");
        } else if (diemSo < 10) {
            System.out.println("Hoc luc : Gioi");
        } else {
            System.out.println("Hoc luc: XS");
        }


        // Switch - case

        // Tên       "A"        "B"     "C";
        //  Họ tên  NVA         TDB     THC;

        String name = "G";
        switch (name) {
            case "A":
                System.out.println("NVA");
                break;
            case "B":
                System.out.println("TDB");
                break;
            case "C":
                System.out.println("THC");
                break;
            default:
                System.out.println("Ten khong ton tai!");
                break;
        }
    }
}
