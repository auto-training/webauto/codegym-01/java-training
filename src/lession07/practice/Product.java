package lession07.practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Product {
    private String name;
    private String description;
    private int quantity;

    public Product(String name, String description, int quantity) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
    }

    public static void main(String[] args) {
        // Khoi tao nha kho - Chua danh sach san pham
        List<Product> inventory = new ArrayList<>();
        List<Map> inventoryList = new ArrayList<>();
        // Tao san pham

        // Object: String, int, long, double, Product
        for (int i = 1; i <= 10; i++) {
            String name = "IPhone " + i;
            if (i % 2 == 0) name = "Samsung" + i;
            Product product = new Product(name, name, i);
            inventory.add(product);

            // Map
            Map<String, Object> productMap = new HashMap<>();
            productMap.put("name", name);
            productMap.put("des", name);
            productMap.put("quantity", i);
            productMap.put("productList", List.of(product));
            inventoryList.add(productMap);
        }

        // Print
        System.out.println(inventory);

        // Tim kiem cac product co chua iphone
        System.out.println("PRINT PRODUCTS ---------");
        int size = inventory.size();

        int total = 0;
        for (int i = 0; i < size; i++) { // i=i+1~ i+=1;
            Product product = inventory.get(i);
            if (product.getName().toLowerCase().contains("iphone") || product.getDescription().toLowerCase().contains("iphone")) {
                System.out.println("Product Index : " + i + " - Product: " + product);
            }

            // Tinh tong cac san pham
            total += product.getQuantity();
            //total = total + product.getQuantity();
        }

        System.out.println("Total : " + total);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
