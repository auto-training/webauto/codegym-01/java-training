package lession07;

import java.util.ArrayList;
import java.util.List;

public class CollectionJava {
    public static void main(String[] args) {
        Student[] arrStudent = new Student[10];                 // Mang 1  chieu
        //1   2   3   4   5   6   7   8   9   10
        //a        a
        List<Student> studentList = new ArrayList<>();          // Danh

        studentList.add(new Student("Le Khoa"));          // index +=1
        studentList.add(0, new Student("Hien"));     // index Khoa + 1 = 1
        // Hien, Le Khoa
        studentList.add(1, new Student("Ha Anh"));
        // Hien, Ha Anh, Le Khoa
        studentList.add(2, new Student("Minh"));


        // Hien, Ha Anh, Minh, LK
        studentList.add(1, new Student("Hung"));
        studentList.add(new Student("Yen"));

        List<Student> bkList = new ArrayList<>();
        bkList.add(new Student("Test"));
        bkList.addAll(studentList);
        System.out.println("BK List:\n" + bkList);

        var yenSt = bkList.stream().filter(s -> s.getName().equals("Yen")).findFirst();

        System.out.println(studentList);

        //-   -   -   -   -   -   -   -   -   -   -   -   -   -
        //1   2   3   3
        //arrStudent[0] = a;
        arrStudent[3] = new Student("Minh");
        // arrStudent[12] = a;

    }
}
