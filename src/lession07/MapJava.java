package lession07;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapJava {
    public static void main(String[] args) {
        Map<String, Object> studentA = new HashMap<>();
        // Ten, Tuoi, DOB, Unit...
        studentA.put("ten", "Ha Anh");
        studentA.put("tuoi", 25);
        studentA.put("dob", "11/11/1998");
        studentA.put("unit", "BK");
        studentA.put("news", new Student("A"));

        // Get info
        System.out.println(studentA.get("ten")); // => Ha Anh
        if (Integer.parseInt(studentA.get("tuoi").toString()) == 25) {

        }

        List<Map> studentList = new ArrayList<>();
        studentList.add(studentA);
    }
}
