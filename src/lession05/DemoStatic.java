package lession05;

import static lession05.StaticClass.*;
//import static lession05.StaticClass.inputTextTo;
//import static lession05.StaticClass.unit;
//import static lession05.StaticClass.city;
//import static lession05.StaticClass.inputTextToComboBox;

public class DemoStatic {
    public static void main(String[] args) {
        StaticClass s = new StaticClass();
        s.clickTo();
        s.inputTextTo();

        inputTextTo();
        unit ="Demo Static";

        city = "HCM";
        inputTextToComboBox();
    }

    private String priVariable ="Private";
    protected String proVariable ="Protected";
    public String pubVariable ="Public";
    String defVariable ="Default";

    public void testMethod01(){
        inputTextToComboBox();
        priVariable ="Private";
    }

    public void testMethod02(){
        testMethod01();
    }


    private void tinhDelta (){

    }

    private void showResult(){

    }

    public void giaiPTBac2(){
        tinhDelta();
        showResult();
    }
}
