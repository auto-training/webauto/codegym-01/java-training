package lession05;

public class ChildClass extends DemoStatic {

    public void accessModifier() {
        DemoStatic demo = new DemoStatic();
        demo.proVariable = "Protected";
        demo.pubVariable = "Public";
        demo.defVariable = "Default";
    }
}
