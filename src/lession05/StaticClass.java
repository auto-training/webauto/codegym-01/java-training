package lession05;

public class StaticClass {

    private String i ="6";
    public static String unit = "PTIT";
    public static String city = "HN";
    public static void main(String[] args) {
        new StaticClass().i = "fda";

        StaticClass.unit = "Demo";
        unit = "Test";
    }

    public static void inputTextTo(){
        unit = "fda";
        new StaticClass().clickTo();
    }

    public static void inputTextToComboBox(){
        unit = "fda";
        new StaticClass().clickTo();
    }

    public void clickTo(){
        inputTextTo();
        clickTo2();

        // Call Private method
       // priVariable ="Private";
        DemoStatic demo = new DemoStatic();
        demo.proVariable ="Protected";
        demo.pubVariable ="Public";
        demo.defVariable ="Default";

        demo.giaiPTBac2();
    }

    public void clickTo2(){
        inputTextTo();
    }
}
