package lession06.practices;

public interface Resizeable {
    void resize(double percent);
}
