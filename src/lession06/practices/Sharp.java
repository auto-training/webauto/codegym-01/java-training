package lession06.practices;

public abstract class Sharp {
    int height;
    int width;

    public Sharp() {
    }

    int getHeight() {
        return this.height;
    }

    void setHeight(int height) {
        this.height = height;
    }

    int getWidth() {
        return this.width;
    }

    void setWidth(int width) {
        this.width = width;
    }

    // Tinh dien tich
    abstract double getArea();

    abstract double getArea(int height, int width);
}
