package lession06.practices;

public class Rectangle extends Sharp implements Resizeable {
    public Rectangle(int height, int width) {
        super();
        this.height = height;
        this.width = width;
    }

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 20);
        //rectangle.getArea();
        rectangle.resize(0.5);

        Circle circle = new Circle();
    }

    @Override
    double getArea() {
        System.out.println("getArea with no info : " + height * width);
        return height * width;
    }

    @Override
    double getArea(int height, int width) {
        System.out.println("getArea : " + height * width);
        return height * width;
    }

    @Override
    public void resize(double percent) {
        if (percent > 0) {
            System.out.println("resize: " + getArea() * percent);
        }
    }
}
