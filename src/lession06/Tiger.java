package lession06;

public class Tiger implements Animal, Flyable{
    @Override
    public void run() {

    }

    @Override
    public void eat() {

    }

    @Override
    public void play() {

    }

    @Override
    public void sound() {
        System.out.println("This is a tiger");
        System.out.println("Tiger : Sound");
    }

    @Override
    public void sleep() {

    }

    public static void main(String[] args) {
        Tiger tiger = new Tiger();
        tiger.sound();
    }

    @Override
    public String fly() {
        return null;
    }
}
