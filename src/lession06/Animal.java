package lession06;

public interface Animal {
    String AUTHOR ="Duy";
    void run();
    void eat();
    void play();
    void sound();
    void sleep();
}
