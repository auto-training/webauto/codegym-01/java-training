package lession06;

public abstract class AnimalAbs {
     String name;
     String category;

    AnimalAbs(String name) {
        System.out.println("AnimalAbs = Class");
        this.name = name;
        System.out.println("AnimalAbs - Name: " + this.name);
    }

    abstract String getName();

    abstract void setName(String name);

    public void setCategory(String category) {
        this.category = category;
    }

    abstract String getColor();
}
