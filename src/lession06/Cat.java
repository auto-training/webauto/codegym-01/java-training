package lession06;

public class Cat extends AnimalAbs implements Animal{

    public Cat(String name) {
        super(name);
    }

    public static void main(String[] args) {
        Cat vnCat = new Cat("Cat");
        vnCat.setName("VNCAT");
        System.out.println(vnCat.getName());
        System.out.println(vnCat.getColor());


        vnCat.sound();
    }

    @Override
    String getName() {
        return this.name;
    }

    @Override
    void setName(String name) {
        this.name = "MEO " + name;
    }

    @Override
    String getColor() {
        return this.name + " Red";
    }

    @Override
    public void run() {
        System.out.println("Cat : run");
    }

    @Override
    public void eat() {
        System.out.println("Cat : eat");
    }

    @Override
    public void play() {
        System.out.println("Cat : play");
    }

    @Override
    public void sound() {
        System.out.println("Cat : meo meo");
    }

    @Override
    public void sleep() {

    }
}
