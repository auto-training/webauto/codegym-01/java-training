package lession06;

public class Dog extends AnimalAbs {
    public Dog(String name) {
        super(name);
        System.out.println("Dog = Class");
    }


    @Override
    String getName() {
        return "The dog name: " + this.name;
        //        return "This is a dog - GET";
    }

    @Override
    void setName(String name) {
        this.name = name;
    }

    @Override
    String getColor() {
        return this.name + " dog " + " Black";
    }


    public static void main(String[] args) {
        Dog vnDog = new Dog("Dog");
        vnDog.setName("VN - DOOOGGG");
        System.out.println(vnDog.getName());

        Dog enDog = new Dog("Dog");
        enDog.setName("EN-DOG");
        System.out.println(enDog.getName());
        System.out.println(enDog.getColor());

    }
}
